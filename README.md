# Méthode de travail MVC #
## Présentation des étapes pour mettre en place sa structure MVC à partir de fichier fournis. ##
***
### Etape 1 ###
***

* Vérifier ses dossiers / APP-DOCUMENTS-NOYAU-WWW

**REMARQUE**

* Le fichier: www\index.php contiendra généralement ces includes:

  * **require_once '../noyau/init.php';** // *$connexion PDO et $zone1=''*
  * **include_once '../app/routeur.php';** // *Hydrater $zone1 grâce à $connexion*
  * **require_once '../app/vues/templates/defaut.php';** // *on affiche $zone1*


***
### Etape 2 ###
***

* Copier le fichier index.html fournit et le coller dans
  app\vues\templates\defaut.php

* Copier les fichiers clients dans www
  C'est à dire le dossier css, js, etc si il y'en a.

***
### Etape 3 ###
***

* Découper en partials le fichier app\vues\templates\defaut.php
dans un nouveau dossier 'partials' qui se trouvera dans app\vues\templates\partials

**EXEMPLE**

- \_head.php qui contiendra que le head.
    - les meta, le title et les link.
- \_nav.php qui contiendra le nav.
    - généralement du début de la balise nav à la fin de celle-ci.
- \_footer.php qui contiendra le footer.
    - généralement du début de la balise footer à la fin de celle-ci.
- \_script.php.
    - Les balises scripts, style chargement jQuery etc.
- \_content.php contiendra le gros de la page son contenu.
    - Souvent on aura un div avec un container prendre du début du div à sa fin.
    - Imaginons qu'on aient des articles prendre la partie des articles, voire si elle est entourée
    par un div ou non. Et mettre une zone dynamique dans celle-ci: <?php echo $content1; ?>
    - Si il y'a une partie sidebar avec des widgets par exemple, prendre celle-ci et la mettre dans un
    nouveau fichier php ex: sidebar.php et faire un include: <?php include '../app/vues/templates/partials/\_sidebar.php'; ?>
    dans le content.php la ou on la enlevé.


### Etape 4 ###
***

- Repérer les zones dynamiques du templates
  et les
  placer dans le fichier: app\vues\templates\defaut.php
  -ou dans le fichier app\vues\templates\partials\_content.php par exmeple cité plus haut.

- Mettre tous les includes dans le fichiers: app\vues\templates\defaut.php
    - <?php include '../app/vues/templates/partials/\_head.php'; ?> *dans les balises head*

    - **TOUTES LES AUTRES DANS LA BALISES BODY**

    - <!-- NAV -->
    - <?php include '../app/vues/templates/partials/\_nav.php'; ?>
    - <!-- CONTENT -->
    - <?php include '../app/vues/templates/partials/\_content.php'; ?>
    - <!-- Footer -->
    - <?php include '../app/vues/templates/partials/\_footer.php'; ?>
    - <!-- SCRIPTS -->
    - <?php include '../app/vues/templates/partials/\_script.php'; ?>
