<?php
/*
  ./app/vues/templates/defaut.php
 */
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include '../app/vues/templates/partials/_head.php'; ?>
  </head>

  <body>
    <!-- NAV -->
    <?php include '../app/vues/templates/partials/_nav.php'; ?>
    <!-- CONTENT -->
    <?php include '../app/vues/templates/partials/_content.php'; ?>
    <!-- Footer -->
    <?php include '../app/vues/templates/partials/_footer.php'; ?>
    <!-- SCRIPTS -->
    <?php include '../app/vues/templates/partials/_script.php'; ?>

  </body>
</html>
